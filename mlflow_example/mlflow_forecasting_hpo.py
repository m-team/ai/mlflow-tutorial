#!/usr/bin/env python
# coding: utf-8

# ## 1. Checking the environment
# 

import sys
print(sys.executable)

# If Tensorflow is installed
import tensorflow as tf
#print(f'Tensorflow version = {tf.__version__}\n')


# If scikit is installed
import sklearn
print("scikit-learn version:", sklearn.__version__)


# 
# ## 2. MLflow Components
# 
# The MLflow Model Registry component is a centralized model store, set of APIs, and UI, to collaboratively manage the 
# full lifecycle of MLflow Models. It provides model lineage (which MLflow Experiment and Run produced the model), 
# model versioning, stage transitions, annotations, and deployment management.
# 
# In this python script-part 1, you use each of the MLflow Model Registry's components to develop and manage a production ML application. 
# This script covers the following topics:
# * Log model experiments with MLflow
# * Register models with the Model Registry
# * Add model description and version the model stage transitions
# * Add tags and alias to models and models versions

# ## 2.1 ML Application Example using MLFlow
# ## Load the dataset
# 
# The following cells load a dataset containing weather data and power output information for a wind farm in the United States. 
# The dataset contains wind direction, wind speed, and air temperature features sampled every eight hours 
# (once at 00:00, once at 08:00, and once at 16:00), as well as daily aggregate power output (power), over several years.
# 

import pandas as pd

# Load your windforecast data
# For example:
data_source_url = "https://github.com/dbczumar/model-registry-demo-notebook/raw/master/dataset/windfarm_data.csv"
wind_farm_data = pd.read_csv(data_source_url, index_col=0)

def serialize_data(data, filename):
    with open(filename, "wb") as f_out:
        pickle.dump(data, f_out)

def get_training_data(wind_farm_data):
  training_data = pd.DataFrame(wind_farm_data["2014-01-01":"2018-01-01"])
  X = training_data.drop(columns="power")
  y = training_data["power"]
  return X, y


def get_validation_data(wind_farm_data):

  validation_data = pd.DataFrame(wind_farm_data["2018-01-01":"2019-01-01"])
  X = validation_data.drop(columns="power")
  y = validation_data["power"]
  return X, y


# ## Train a power forecasting model and track it with MLflow
# 
# The following cells train a neural network to predict power output based on the weather features in the dataset. 
# MLflow is used to track the model's hyperparameters, performance metrics, source code, and artifacts.
# 

# #### 2. Define a power forecasting model using RandomForest
from urllib3.util.retry import Retry
import mlflow.sklearn
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error


# Check for available physical devices
gpus = tf.config.list_physical_devices("GPU")
cpus = tf.config.list_physical_devices("CPU")

if len(gpus) > 0:
    print(gpus)
else:
    print("NO GPUS FOUND! Using only CPUs!")

print(f'\n{cpus}\n')


# #### MLflow part
# 
# **! Configure IMPORTANT CONSTANTS !:**

#set the environmental vars to allow 'mlflow_user' to track experiments using MLFlow
import os
import getpass

# IMPORTANT CONSTANTS TO DEFINE
# MLFLOW CREDENTIALS (Nginx). PUT REAL ONES!
# for direct API calls via HTTP we need to inject credentials
MLFLOW_TRACKING_USERNAME = input('Enter your username: ')
MLFLOW_TRACKING_PASSWORD =  getpass.getpass()  # inject password by typing manually
# for MLFLow-way we have to set the following environment variables
os.environ['MLFLOW_TRACKING_USERNAME'] = MLFLOW_TRACKING_USERNAME
os.environ['MLFLOW_TRACKING_PASSWORD'] = MLFLOW_TRACKING_PASSWORD


import subprocess
# Run the git command and capture the output
result = subprocess.run(["git", "config", "--get", "remote.origin.url"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

# Check if the command was successful
if result.returncode == 0:
    # Get the output of the command
    git_url = result.stdout

    # Write the output to a text file
    with open("mlflow_example/links.txt", "w") as f:
        lines = ["Git Repo URL: ", git_url, "\n", "Dataset URL: ", data_source_url,"\n" ]
        f.writelines(lines)
        f.close()
else:
    # Handle any errors or log them
    error_message = result.stderr
    print(f"Error: {error_message}")

import contextlib


# Remote MLFlow server
MLFLOW_REMOTE_SERVER="https://mlflow.cloud.ai4eosc.eu" 

#Set the MLflow server and backend and artifact stores
mlflow.set_tracking_uri(MLFLOW_REMOTE_SERVER)

# Name of the experiment (e.g. name of the  code repository)
MLFLOW_EXPERIMENT_NAME="wind_power_forecast_W"

#set an experiment name for all different runs
mlflow.set_experiment(MLFLOW_EXPERIMENT_NAME)

# Train the model and use MLflow to log and track its parameters, metrics, artifacts, and source code.

import mlflow
import mlflow.tensorflow
from mlflow.data.pandas_dataset import PandasDataset

import pickle
import optuna
import os
from optuna.samplers import TPESampler
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error

def load_pickle(filename):
    with open(filename, "rb") as f_in:
        return pickle.load(f_in)

def objective(trial, X_train, y_train, X_val, y_val):
    params = {
        'n_estimators': trial.suggest_int('n_estimators', 10, 50, 1),
        'max_depth': trial.suggest_int('max_depth', 1, 20, 1),
        'min_samples_split': trial.suggest_int('min_samples_split', 2, 10, 1),
        'min_samples_leaf': trial.suggest_int('min_samples_leaf', 1, 4, 1),
        'random_state': 42,
        'n_jobs': -1
    }

    rf = RandomForestRegressor(**params)
    rf.fit(X_train, y_train)
    y_pred = rf.predict(X_val)
    rmse = mean_squared_error(y_val, y_pred, squared=False)

    X_train, y_train = get_training_data(wind_farm_data)

    #Log the dataset
    #dataset: PandasDataset = mlflow.data.from_pandas(wind_farm_data, source=data_source_url)

    # Log parameters and metrics using MLflow
    mlflow.log_params(params)
    mlflow.log_metric("rmse", rmse)
    mlflow.sklearn.log_model(rf, artifact_path="artifacts")
    #mlflow.log_input(dataset, context="training")

    return rmse

from datetime import datetime
dt = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def run_optimization(data_path: str = "mlflow_example/data_path", num_trials: int = 10):
    X_train, y_train = load_pickle(os.path.join(data_path, "train.pkl"))
    X_val, y_val = load_pickle(os.path.join(data_path, "val.pkl"))

    # Start a nested run for the entire optimization process
    with mlflow.start_run(run_name=f"sklearn_HPO_{dt}", nested=True):
        sampler = TPESampler(seed=42)
        study = optuna.create_study(direction="minimize", sampler=sampler)

        for _ in range(num_trials):
            # Start a child run for each trial
            with mlflow.start_run(run_name=f"trial_{_}", nested=True):
                trial = study.ask()
                result = objective(trial, X_train, y_train, X_val, y_val)
                study.tell(trial, result)
                mlflow.log_metric("trial_rmse", result)

    # End the nested run for the entire optimization process
    mlflow.end_run()


if __name__ == '__main__':
       
    X_train, y_train = get_training_data(wind_farm_data)
    X_val, y_val = get_validation_data(wind_farm_data)

    # Specify the new directory name
    path_dir = "mlflow_example/data_path/"

    # Create the new directory
    os.makedirs(path_dir, exist_ok=True)

    # Serialize the data using pickle
    serialize_data((X_train, y_train), f"{path_dir}train.pkl")
    serialize_data((X_val, y_val), f"{path_dir}val.pkl")

    #Log the dataset ready for download
    # Save the DataFrame to a CSV file
    data_csv = "windfarm_data.csv"
    wind_farm_data.to_csv(data_csv, index=False)

    # Log the CSV file as an artifact in MLflow
    mlflow.log_artifact(data_csv, artifact_path='source-files/data/dataset')

    # End the existing run if there is one
    if mlflow.active_run():
        mlflow.end_run()

    # Call run_optimization with specified parameters
    run_optimization(data_path="mlflow_example/data_path", num_trials=5)

