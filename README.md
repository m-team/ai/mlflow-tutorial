# MLFlow Tutorial

## Getting started

This is a basic tutorial intorducing you into MLFlow framework

## Prerequisites

### Main installation
you have:
* python3 > 3.7 but lower than 3.11
* pip is installed

### (Optional) Install favorite virtual environment
For example:
* `sudo apt install python3-virtualenv`
* `python3 -m venv path/to/mlflow-venv`
* `source mlflow-venv/bin/activate`
* (to stop virtualenv) `deactivate`

### Install jupyter
* jupyter has to be installed to run notebooks: e.g. `pip install jupyterlab` or `pip install notebook`

### Install dependencies from the requirements.txt
* run `pip install -r requirements.txt`

The code uses in the notebook uses pandas, matplotlib, tensorflow but all that packages should be installed when you install tensorflow itself.

## Jupyter Notebook

Start the provided notebook `mlflow_recasting_app_vX.ipynb` and follow the steps.

## Acknowledgment
This work is co-funded by AI4EOSC project that has received funding from the European Union's Horizon Europe 2022 research and innovation programme under agreement No 101058593

## License
For open source projects, say how it is licensed.

## Tutorial step-by-step how to organize and track your experiments in MLflow
Check this presentation here: https://indico.scc.kit.edu/event/3845/contributions/14701/attachments/6987/11043/1.6_MLflow_and_its_usage.pdf

Slide examples:

![img-1](images/5-1.6%20MLflow%20and%20its%20usage.jpg) 

